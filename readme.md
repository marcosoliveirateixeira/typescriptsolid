SOLID

S - Single Responsability Principle
 - Cada classe deve ter apenas um motivo para mudar
O - Open closed Principle
 - Módulos, classes, objetos e operações devem estar abertos para extensão, mas fechados para modificaçoes
L - Liskov Substitution Principle

I - Interface segregation principle
 - os clientes nao devem ser forçados a depender de interfaces que nao utilizam
D - Dependency inversion principle
 - Módulos de alto nível nao devem ser dependentes de módulos de baixo nivel, ambos devem depender de abstraçao
